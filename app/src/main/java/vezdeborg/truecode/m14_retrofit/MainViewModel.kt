package vezdeborg.truecode.m14_retrofit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import vezdeborg.truecode.m14_retrofit.api.RetrofitServices

class MainViewModel: ViewModel() {

    private val _state = MutableStateFlow<State>(State.Loading)
    val state = _state.asStateFlow()

    init { tryLoadUser() }

    fun tryLoadUser() {
        viewModelScope.launch {
            _state.value = State.Loading
            try {
                val userData = RetrofitServices.getUserApi.getUserData()
                _state.value = State.Success(userData)
            } catch (t: Throwable) {
                _state.value = State.Error(t)
            }
        }
    }
}