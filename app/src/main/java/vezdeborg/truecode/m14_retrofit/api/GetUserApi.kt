package vezdeborg.truecode.m14_retrofit.api

import retrofit2.http.GET
import vezdeborg.truecode.m14_retrofit.data.UserModel

interface GetUserApi {
    @GET("api/")
    suspend fun getUserData(): UserModel
}

