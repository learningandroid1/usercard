package vezdeborg.truecode.m14_retrofit

import vezdeborg.truecode.m14_retrofit.data.UserModel

sealed class State {
    class Success(val model: UserModel): State()
    object Loading: State()
    class Error(val error: Throwable): State()
}
