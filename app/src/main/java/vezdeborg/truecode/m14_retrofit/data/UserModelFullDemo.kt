package vezdeborg.truecode.m14_retrofit.data

data class UserModelFullDemo(
    val results: List<ResultsFD>,
    val info: Info
)

data class Info(
    val page: Int,
    val results: Int,
    val seed: String,
    val version: String
)

data class ResultsFD(
    val gender: String,
    val name: NameFD,
    val location: LocationFD,
    val email: String,
    val login: LoginFD,
    val dob: DobFD,
    val registered: RegisteredFD,
    val phone: String,
    val cell: String,
    val id: IdFD,
    val picture: PictureFD,
    val nat: String
)

data class DobFD(
    val age: Int,
    val date: String
)

data class IdFD(
    val name: String,
    val value: String
)

data class LocationFD(
    val city: String,
    val coordinates: CoordinatesFD,
    val country: String,
    val postcode: Int,
    val state: String,
    val street: StreetFD,
    val timezone: TimezoneFD
)

data class LoginFD(
    val md5: String,
    val password: String,
    val salt: String,
    val sha1: String,
    val sha256: String,
    val username: String,
    val uuid: String
)

data class NameFD(
    val first: String,
    val last: String,
    val title: String
)

data class PictureFD(
    val large: String,
    val medium: String,
    val thumbnail: String
)

data class RegisteredFD(
    val age: Int,
    val date: String
)

data class CoordinatesFD(
    val latitude: String,
    val longitude: String
)

data class StreetFD(
    val name: String,
    val number: Int
)

data class TimezoneFD(
    val description: String,
    val offset: String
)
