package vezdeborg.truecode.m14_retrofit.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserModel(
    @Json(name = "results") val results: List<Results>,
)

@JsonClass(generateAdapter = true)
data class Results(
    @Json(name = "gender") val gender: String,
    @Json(name = "name") val name: Name,
    @Json(name = "location") val location: Location,
    @Json(name = "email") val email: String,
    @Json(name = "dob") val dob: Dob,
    @Json(name = "phone") val phone: String,
    @Json(name = "picture") val picture: Picture,
)

@JsonClass(generateAdapter = true)
data class Name(
    @Json(name = "first") val first: String,
    @Json(name = "last") val last: String,
)

@JsonClass(generateAdapter = true)
data class Location(
    @Json(name = "country") val country: String,
)

@JsonClass(generateAdapter = true)
data class Dob(
    @Json(name = "age") val age: Int,
)

@JsonClass(generateAdapter = true)
data class Picture(
    @Json(name = "large") val large: String,
)
