package vezdeborg.truecode.m14_retrofit.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vezdeborg.truecode.m14_retrofit.MainViewModel
import vezdeborg.truecode.m14_retrofit.R
import vezdeborg.truecode.m14_retrofit.api.RetrofitServices
import vezdeborg.truecode.m14_retrofit.data.UserModel
import vezdeborg.truecode.m14_retrofit.databinding.ActivityMainBinding
import androidx.activity.viewModels
import androidx.lifecycle.viewModelScope
import vezdeborg.truecode.m14_retrofit.State

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        this.lifecycleScope.launchWhenStarted {
            viewModel.state.collect { state ->
                when (state) {
                    is State.Loading -> {
                        binding.progressBar.visibility = View.VISIBLE
                        binding.refreshButton.isEnabled = false
                    }
                    is State.Success -> {
                        binding.progressBar.visibility = View.GONE
                        binding.refreshButton.isEnabled = true
                        launch(Dispatchers.Main) {
                            displayUserData(state.model)
                        }
                    }
                    is State.Error -> {
                        binding.progressBar.visibility = View.GONE
                        binding.refreshButton.isEnabled = true
                        Toast.makeText(this@MainActivity, state.error.toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

        binding.refreshButton.setOnClickListener {
            viewModel.tryLoadUser()
        }
    }

    private fun displayUserData(userData: UserModel) {
        Glide
            .with(this)
            .load(userData.results[0].picture.large)
            .into(binding.userImage)
        //binding.userImage.setImageResource(R.drawable.ic_launcher_foreground)
        binding.userName.text = resources.getString(R.string.user_name, userData.results[0].name.first, userData.results[0].name.last)
        binding.userGender.text = resources.getString(R.string.user_gender, userData.results[0].gender)
        binding.userAge.text = resources.getString(R.string.user_age, userData.results[0].dob.age.toString())
        binding.userLocation.text = resources.getString(R.string.user_country, userData.results[0].location.country)
        binding.userEmail.text = resources.getString(R.string.user_email, userData.results[0].email)
        binding.userPhone.text = resources.getString(R.string.user_phone, userData.results[0].phone)
    }
}